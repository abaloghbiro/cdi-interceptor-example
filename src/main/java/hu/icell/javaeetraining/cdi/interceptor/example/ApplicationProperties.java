package hu.icell.javaeetraining.cdi.interceptor.example;

import javax.enterprise.context.Dependent;

import hu.icell.javaeetraining.cdi.extension.propertyhandler.Property;
import hu.icell.javaeetraining.cdi.extension.propertyhandler.PropertyFile;

@Dependent
@PropertyFile(fileName = "application.properties")
public class ApplicationProperties {

	@Property(propertyName = "appname")
	private String appName;

	@Property(propertyName = "maxcachesize")
	private Integer cacheSize;

	@Property(propertyName = "randomizationvalue")
	private Integer randomizationvalue;

	@Property(propertyName = "doubletest")
	private Double doubleTest;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public Integer getCacheSize() {
		return cacheSize;
	}

	public void setCacheSize(Integer cacheSize) {
		this.cacheSize = cacheSize;
	}

	public Integer getRandomizationvalue() {
		return randomizationvalue;
	}

	public void setRandomizationvalue(Integer randomizationvalue) {
		this.randomizationvalue = randomizationvalue;
	}

	public Double getDoubleTest() {
		return doubleTest;
	}

	public void setDoubleTest(Double doubleTest) {
		this.doubleTest = doubleTest;
	}

	@Override
	public String toString() {
		return "ApplicationProperties [appName=" + appName + ", cacheSize=" + cacheSize + ", randomizationvalue="
				+ randomizationvalue + ", doubleTest=" + doubleTest + ", getAppName()=" + getAppName()
				+ ", getCacheSize()=" + getCacheSize() + ", getRandomizationvalue()=" + getRandomizationvalue()
				+ ", getDoubleTest()=" + getDoubleTest() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
