package hu.icell.javaeetraining.cdi.interceptor.example;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

@Startup
@Singleton
public class InterceptorTester {

	@Inject
	private NotUsefulBean observed;

	@PostConstruct
	public void init() {
		try {
			observed.executeNotUsefulCalculation();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
