package hu.icell.javaeetraining.cdi.interceptor.example;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class NotUsefulBean {

	@Inject
	private ApplicationProperties appHolder;

	@PerformanceMeasurement
	public void executeNotUsefulCalculation() throws InterruptedException {

		System.out.println(appHolder.toString());
		System.out.println("We will do a hard calculation... RANDOM NUM : " + appHolder.getRandomizationvalue());
		int sleepValue = (int) (Math.random() * appHolder.getRandomizationvalue());
		Thread.sleep(sleepValue);
	}
}
