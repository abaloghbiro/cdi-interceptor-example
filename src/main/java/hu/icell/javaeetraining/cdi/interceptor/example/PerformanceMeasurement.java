package hu.icell.javaeetraining.cdi.interceptor.example;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.interceptor.InterceptorBinding;

@Inherited
@InterceptorBinding
@Retention(RUNTIME)
@Target(value = {ElementType.METHOD, ElementType.TYPE})
public @interface PerformanceMeasurement {

}
