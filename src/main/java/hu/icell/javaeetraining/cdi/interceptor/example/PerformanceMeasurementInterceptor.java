package hu.icell.javaeetraining.cdi.interceptor.example;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Interceptor
@PerformanceMeasurement
public class PerformanceMeasurementInterceptor {

	@AroundInvoke
	public Object measurePerformance(InvocationContext context) throws Exception {
		
		long startTime = System.currentTimeMillis();
	
		Object ret = context.proceed();
		
		long endTime =  System.currentTimeMillis();
		
		System.out.println("Intercepted method executed under "+(endTime - startTime)+ " ms.");
		
		return ret;
	}
}
